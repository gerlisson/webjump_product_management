# Teste Backend Web Jump?
O projeto consiste no cadastro, edição e exclusão de categorias e produtos CRUD.

## Desenvolvimento
Para o desenvolvimento foi utilizado as seguintes ferramentas e tecnologias:
- PHP 7.0
- HTML
- CSS
- JQuery
- Javascript
- AJAX
- Composer
- Doctrine 2
- PHPStorm (editor)

##Como rodar a aplicação
1. Importar o arquivo **_/database/database.sql_**
2. Configurar os dados de acesso ao banco de dados no arquivo **_/config/local.php_**
3. Fazer downlod **_composer_**
4. Gerar as proxies **_php vendor/doctrine/orm/bin/doctrine orm:generate-proxies_**

## Observações
1. Devido à um problema pessoal não foi possível fazer o importador .CSV por falta de tempo;
2. A inicialização da aplicação está sendo feita a partir do arquivo config.php mas normalmente utilizo psr-4;
3. Foi implementado o upload e imagem conforme o desafio.
