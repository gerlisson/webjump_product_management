<?php
use Doctrine\ORM\Tools\Console\ConsoleRunner;

include("config.php");

$entityManager = registry::resolve("entityManager");

return ConsoleRunner::createHelperSet($entityManager);