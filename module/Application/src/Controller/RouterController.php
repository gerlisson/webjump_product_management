<?php
/**
 * Created by PhpStorm.
 * User: Gerlisson
 * Date: 28/03/2019
 * Time: 10:20
 */

namespace RouterController;

use CategoryController\CategoryController;
use ProductController\ProductController;

class RouterController
{
    protected $controller;

    /**
     * RouterController constructor.
     */
    public function __construct($get)
    {
        switch (count($get)) {

            case 1:

                if (stristr($get[0], 'categories')) {
                    return new CategoryController($get);
                    break;
                }
                if (stristr($get[0], 'products')) {
                    return new ProductController($get);
                    break;
                }
                break;

            case 2:

                if (stristr($get[0], 'categories')) {
                    return new CategoryController($get);
                    break;
                }
                if (stristr($get[0], 'products')) {
                    return new ProductController($get);
                    break;
                }
                break;

            default:

                break;
        }
    }
}