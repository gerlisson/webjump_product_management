<?php
/**
 * Created by PhpStorm.
 * User: Gerlisson
 * Date: 24/03/2019
 * Time: 17:36
 */

namespace GlobalsController;

class GlobalsController
{
    private $em;
    private $bd;

    public function __construct()
    {
        $this->setEm(\registry::resolve("entityManager"));
    }

    /**
     * @return mixed
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @param mixed $em
     */
    public function setEm($em)
    {
        $this->em = $em;
    }

    /**
     * @return mixed
     */
    public function getBd()
    {
        return $this->bd;
    }

    /**
     * @param mixed $bd
     */
    public function setBd($bd)
    {
        $this->bd = $bd;
    }
}