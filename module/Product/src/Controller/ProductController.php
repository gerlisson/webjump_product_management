<?php
/**
 * Created by PhpStorm.
 * User: Gerlisson
 * Date: 28/03/2019
 * Time: 11:44
 */

namespace ProductController;

use Category;
use Product;

use GlobalsController\GlobalsController;

class ProductController extends GlobalsController
{
    public function __construct($get)
    {
        parent::__construct();

        if(! isset($get[1])){

            $this->listAll();

        }else{
            $this->addEdit($get);

        }
    }

    public function addEdit($get)
    {
        $data['categories'] = $this->getEm()->getRepository("Category")->findBy(array(), array('name' => 'asc'));

        $product = $this->getEm()->getRepository("Product")->find($get[1]);
        if(count($product)){
            $data['id'] = $product->getId();
            $data['name'] = $product->getName();
            $data['sku'] = $product->getSku();
            $data['price'] = value($product->getPrice());
            $data['description'] = $product->getDescription();
            $data['amount'] = $product->getAmount();
            $data['image'] = PRODUCT . $product->getImage();

            $categoriesProduct = array();
            foreach($product->getCategories() as $category){
                $categoriesProduct[] = $category->getId();
            }
            $data['categoriesProduct'] = $categoriesProduct;
        }


        $this->setBd($data);

        include(__DIR__ . '/../view/product/add-edit.phtml');
    }

    public function listAll()
    {
        $data['products'] = $this->getEm()->getRepository("Product")->findBy(array(), array('name' => 'asc'));

        $this->setBd($data);

        include(__DIR__ . '/../view/product/list.phtml');
    }
}