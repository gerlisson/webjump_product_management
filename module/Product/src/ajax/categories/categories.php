<?php

include('../../../../../config/config.php');
include(DIR_CLASS . 'functions.php');

$em = Registry::resolve("entityManager");

try{
    if(!isset($_POST['name']) || isset($_POST['name']) && $_POST['name'] == ''){
        throw new \Exception('Campo nome inválido.');
    }
    if(!isset($_POST['code']) || isset($_POST['code']) && $_POST['code'] == ''){
        throw new \Exception('Campo código inválido.');
    }

    $edit = false;

    if(isset($_POST['edit']) && $_POST['edit'] != ''){
        $edit = true;

        $category = $em->getRepository('Category')->find($_POST['edit']);
        $category->setName(trim($_POST['name']));
        $category->setCode(trim($_POST['code']));
        $category->setSlug(slug($_POST['name']));

    }else{
        // __construct($name, $code)
        $category = new Category(
            trim($_POST['name']),
            trim($_POST['code']),
            slug($_POST['name'])
        );
    }

    $em->persist($category);
    $em->flush();

    print json_encode([
        'success'   => true,
        'edit' => $edit,
        'message'   => '<div class="alert alert-success"><p>Salvo com sucesso!</p></div>'
    ]);

}catch (\Exception $e){

    print json_encode([
        'success'   => true,
        'message'   => '<div class="alert alert-error"><p>'.$e->getMessage().'</p></div>'
    ]);

}catch (\PDOException $e){

    print json_encode([
        'success'   => true,
        'message'   => '<div class="alert alert-error"><p>'.$e->getMessage().'</p></div>'
    ]);

}