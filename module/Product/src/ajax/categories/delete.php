<?php

include('../../../../../config/config.php');
include(DIR_CLASS . 'functions.php');

$em = Registry::resolve("entityManager");

try{
    if(!isset($_GET['id']) || isset($_GET['id']) && !is_numeric($_GET['id'])){
        throw new \Exception('Categoria inválida');
    }

    $category = $em->getRepository('Category')->find($_GET['id']);
    $em->remove($category);
    $em->flush();

    print json_encode([
        'success'   => true
    ]);

}catch(\Exception $e){
    print json_encode([
        'success'   => false,
        'message'   => $e->getMessage()
    ]);

}catch(\PDOException $e){
    print json_encode([
        'success'   => false,
        'message'   => $e->getMessage()
    ]);
}