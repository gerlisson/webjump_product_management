<?php

include('../../../../../config/config.php');
include(DIR_CLASS . 'functions.php');

$em = Registry::resolve("entityManager");

try{
    if(!isset($_GET['id']) || isset($_GET['id']) && !is_numeric($_GET['id'])){
        throw new \Exception('Produto inválido');
    }

    $product = $em->getRepository('Product')->find($_GET['id']);
    @unlink(DIR_PRODUCT . $product->getImage());

    // remove categories
    foreach($product->getCategories() as $category):
        $category = $em->getRepository("Category")->find($category);
        $product->getCategories()->removeElement($category);
        $em->flush();
    endforeach;

    $em->remove($product);
    $em->flush();

    print json_encode([
        'success'   => true
    ]);

}catch(\Exception $e){
    print json_encode([
        'success'   => false,
        'message'   => $e->getMessage()
    ]);

}catch(\PDOException $e){
    print json_encode([
        'success'   => false,
        'message'   => $e->getMessage()
    ]);
}