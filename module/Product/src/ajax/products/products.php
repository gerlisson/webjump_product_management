<?php

include('../../../../../config/config.php');
include(DIR_CLASS . 'functions.php');
include(DIR_CLASS . 'class_upload/class.upload.php');

$em = Registry::resolve("entityManager");

try{
    if(!isset($_POST['sku']) || isset($_POST['sku']) && $_POST['sku'] == ''){
        throw new \Exception('Campo SKU inválido.');
    }
    if(!isset($_POST['name']) || isset($_POST['name']) && $_POST['name'] == ''){
        throw new \Exception('Campo nome inválido.');
    }
    if(!isset($_POST['price']) || isset($_POST['price']) && $_POST['price'] == ''){
        throw new \Exception('Campo preço inválido.');
    }
    if(!isset($_POST['quantity']) || isset($_POST['quantity']) && $_POST['quantity'] == ''){
        throw new \Exception('Campo quantidade inválido.');
    }
    if(!isset($_POST['description']) || isset($_POST['description']) && $_POST['description'] == ''){
        throw new \Exception('Campo descrição inválido.');
    }
    if(count($_POST['categories']) == 0){
        throw new \Exception('Selecione pelo menos uma categoria.');
    }

    $edit = false;

    if(isset($_POST['edit']) && $_POST['edit'] != ''){
        $edit = true;

        $product = $em->getRepository('Product')->find($_POST['edit']);
        $product->setName(trim($_POST['name']));
        $product->setSku(trim($_POST['sku']));
        $product->setPrice(savePrice($_POST['price']));
        $product->setDescription(trim($_POST['description']));
        $product->setAmount(trim($_POST['quantity']));
        $product->setSlug(slug($_POST['name']));

        $handler = new \Upload($_FILES['image']);
        if ($handler->uploaded) {
            @unlink(DIR_PRODUCT . $product->getImage());

            addImage($handler, slug($_POST['name']), $product);
        }

        // remove categories
        foreach($product->getCategories() as $category):
            $category = $em->getRepository("Category")->find($category);
            $product->getCategories()->removeElement($category);
            $em->flush();
        endforeach;

        addCategories($_POST['categories'], $product, $em);

    }else{

        //__construct($name, $sku, $price, $description, $amount)
        $product = new Product(
            trim($_POST['name']),
            trim($_POST['sku']),
            savePrice($_POST['price']),
            trim($_POST['description']),
            $_POST['quantity'],
            slug($_POST['name'])
        );

        // load image
        $handler = new \Upload($_FILES['image']);
        if (!$handler->uploaded) {
            throw new \Exception("Selecione a imagem principal.");
        }
        addImage($handler, slug($_POST['name']), $product);

        addCategories($_POST['categories'], $product, $em);
    }

    $em->persist($product);
    $em->flush();

    print json_encode([
        'success'   => true,
        'edit' => $edit,
        'message'   => '<div class="alert alert-success"><p>Salvo com sucesso!</p></div>'
    ]);

}catch (\Exception $e){

    print json_encode([
        'success'   => true,
        'message'   => '<div class="alert alert-error"><p>'.$e->getMessage().'</p></div>'
    ]);

}catch (\PDOException $e){

    print json_encode([
        'success'   => true,
        'message'   => '<div class="alert alert-error"><p>'.$e->getMessage().'</p></div>'
    ]);

}

function addImage($handler, $title, $product) {
    $handler->image_resize = false;
    $handler->file_new_name_body = $title;
    $handler->Process(DIR_PRODUCT);

    $handler->Clean();

    $product->setImage($handler->file_dst_name);
}

function addCategories($categories, $product, $em) {
    $categoriesArray = array();
    foreach($categories as $sub){
        $category = $em->getRepository('Category')->find($sub);
        $categoriesArray[] = $category;
    }
    $product->setCategories($categoriesArray);
}